import 'dart:html';
import 'package:angular2/router.dart';

Map<String, String> lat_cyr = {
  KeyCode.A:"А",
  KeyCode.B:"Б",
  KeyCode.V:"В",
  KeyCode.G:"Г",
  KeyCode.D:"Д",
  KeyCode.CLOSE_SQUARE_BRACKET:"Ђ",
  KeyCode.E:"Е",
  KeyCode.BACKSLASH:"Ж",
  KeyCode.Z:"З",
  KeyCode.I:"И",
  KeyCode.J:"Ј",
  KeyCode.K:"К",
  KeyCode.L:"Л",
  KeyCode.Q:"Љ",
  KeyCode.M:"М",
  KeyCode.N:"Н",
  KeyCode.W:"Њ",
  KeyCode.O:"О",
  KeyCode.P:"П",
  KeyCode.R:"Р",
  KeyCode.S:"С",
  KeyCode.T:"Т",
  KeyCode.SINGLE_QUOTE:"Ћ",
  KeyCode.U:"У",
  KeyCode.F:"Ф",
  KeyCode.H:"Х",
  KeyCode.C:"Ц",
  KeyCode.SEMICOLON:"Ч",
  KeyCode.X:"Џ",
  KeyCode.OPEN_SQUARE_BRACKET:"Ш"
};

Map config = null;
Router appRouter = null;

var onBeforeWindowUnloadSub = null, onKeydownSub = null;
