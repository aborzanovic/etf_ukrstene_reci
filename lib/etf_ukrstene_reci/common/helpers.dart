import 'dart:async';
import 'dart:isolate';
import 'dart:js' as js;
import 'dart:convert';
import 'dart:html';
import 'package:etf_ukrstene_reci/crossword_gen/lib/crossword_gen.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/common/globals.dart' as globals;



displayMessages(Map<String, dynamic> serverResponse){
  var jQ = js.context['jQuery'];
  serverResponse["messages"].forEach(
      (val) => jQ.callMethod('notify', [val["message"], val["type"]])
  );
}

displayMessage(String message, String type){
  var jQ = js.context['jQuery'];
  jQ.callMethod('notify', [message, type]);
}

Future<Map<String, dynamic>> sendPost(Map<String, dynamic> data, String url) async{
  var completer = new Completer();
  HttpRequest request = new HttpRequest();

  request.onReadyStateChange.listen((_) {
    if (request.readyState == HttpRequest.DONE) {
      if (request.status == 200 || request.status == 0) {
        Map<String, dynamic> decodedResponse =JSON.decode(request.responseText);
        displayMessages(decodedResponse);
        completer.complete(decodedResponse);
      }
      else
        completer.complete(null);
    }
  });
  request.open("POST", url);
  if(window.localStorage["token"] != null)
    data["token"] = window.localStorage["token"];
  else
    data["token"] = "";
  request.send(JSON.encode(data));
  return completer.future;
}

String convertKeyCodeToCyrillic(int code){
  return globals.lat_cyr[code];
}

navigate(List<String> arguments){
  if(globals.onKeydownSub != null)
    globals.onKeydownSub.cancel();
  globals.onKeydownSub = null;
  if(globals.appRouter != null)
    globals.appRouter.navigate(arguments);
}

bool isDartVM(){
  return !identical(1, 1.0);
}