import 'dart:async';

import 'dart:collection';
import 'dart:convert';
import 'dart:html';
import 'package:angular2/core.dart';

import 'package:etf_ukrstene_reci/etf_ukrstene_reci/common/globals.dart' as globals;
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/common/helpers.dart' as helpers;
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/models/board.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/models/game.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/providers/board_service.dart';

@Injectable()
class GameService {
  final BoardService _boardService;

  GameService(this._boardService);

  Future<Game> getRandomGame() async{
    var completer = new Completer();
    Game game;

    String response = await HttpRequest.getString(
            globals.config["serverBasePath"] +
            globals.config["gameGetRandomRoute"]
    );
    Map<String, dynamic> decodedResponse = JSON.decode(response);

    Board board = await _boardService.getBoardConfiguration(decodedResponse["game"]["board_id"]);
    HashMap<int, String> letters = new HashMap();
    HashMap<String, String> horizontalTooltips = new HashMap();
    HashMap<String, String> verticalTooltips = new HashMap();

    int index = 0;
    List<String> lettersList = decodedResponse["game"]["letters"].split(",");

    for(String letter in lettersList){
      letters[index++] = letter;
    }

    Map tooltips = decodedResponse["game"]["tooltips"];
    if(tooltips[0] is List) {
      int tooltipCnt = 0;
      (tooltips[0] as List).forEach(
          (tooltip) {
            horizontalTooltips[tooltipCnt.toString()] = tooltip;
            tooltipCnt++;
          }
      );
    }
    else
      horizontalTooltips = tooltips[0];
    if(tooltips[1] is List) {
      int tooltipCnt = 0;
      (tooltips[1] as List).forEach(
          (tooltip) {
            horizontalTooltips[tooltipCnt.toString()] = tooltip;
            tooltipCnt++;
          }
      );
    }
    else
      verticalTooltips = tooltips[1];

    game  = new Game(decodedResponse["game"]["id"], board, letters, horizontalTooltips, verticalTooltips);

    completer.complete(game);

    return completer.future;
  }

  Future<Null> uploadGame(List<String> letters, List<int> blackFields, int dimension) async{
    var completer = new Completer();

    Map<String, dynamic> data = new Map<String, dynamic>();
    data["letters"] = letters;
    data["blackFields"] = blackFields;
    data["dimension"] = dimension;

    await helpers.sendPost(data, globals.config["serverBasePath"] + globals.config["gameAddRoute"]);

    completer.complete(null);
    return completer.future;
  }
}