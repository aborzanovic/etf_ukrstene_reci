import 'dart:async';

import 'dart:convert';
import 'dart:html';
import 'package:angular2/core.dart';

import 'package:etf_ukrstene_reci/etf_ukrstene_reci/common/globals.dart' as globals;
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/common/helpers.dart' as helpers;
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/models/board.dart';

@Injectable()
class BoardService {

  Future<bool> checkField(int id, int num, String char) async{
    var completer = new Completer();

    Map<String, dynamic> data = new Map<String, dynamic>();
    data["id"] = id;
    data["num"] = num;
    data["char"] = char;

    Map<String, dynamic> response =
      await helpers.sendPost(data, globals.config["serverBasePath"] + globals.config["boardCheckFieldRoute"]);

    completer.complete(response == null ? false : response["valid"]);

    return completer.future;
  }

  Future<Board> getBoardConfiguration(int id) async{
    var completer = new Completer();
    Board board;

    Map<String, dynamic> data = new Map<String, dynamic>();
    data["id"] = id;

    Map<String, dynamic> response =
    await helpers.sendPost(data, globals.config["serverBasePath"] + globals.config["boardGetConfigurationRoute"]);
    board  = new Board(response["board"]["dimension"]);
    response["board"]["blackFields"].forEach(
        (val) => board.addBlackField(val)
    );
    completer.complete(board);

    return completer.future;
  }
}