import 'dart:html';
import 'dart:convert';
import 'dart:async';

import 'package:angular2/core.dart';

import 'package:etf_ukrstene_reci/etf_ukrstene_reci/common/globals.dart' as globals;
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/common/helpers.dart' as helpers;

  @Injectable()
  class AuthService {

    Future<Map<String, String>> getToken(String username,
        String password) async{
      var completer = new Completer();

      Map<String, dynamic> data = new Map<String, dynamic>();
      data["username"] = username;
      data["password"] = password;

      Map<String, dynamic> response =
        await helpers.sendPost(data, globals.config["serverBasePath"] + globals.config["authCreateRoute"]);
      Map<String, String> ret = new Map<String, String>();
      ret["token"] = response["token"];
      ret["user_name"] = response["user_name"];
      ret["isAdmin"] = response["isAdmin"];

      completer.complete(response == null ? null : ret);

      return completer.future;
    }

    Future<Map<String, bool>> logIn(String username,
        String password) async{
      var completer = new Completer();

      Map<String, String> token = await getToken(username, password);
      window.localStorage["token"] = token["token"];
      window.localStorage["user_name"] = token["user_name"];
      window.localStorage["isAdmin"] = (token["isAdmin"] == 1).toString();

      Map<String, bool> ret = new Map();
      ret["success"] = token != null;
      ret["isAdmin"] = token["isAdmin"] == 1;
      completer.complete(ret);

      return completer.future;
    }

    Future<bool> createAccount(String username,
        String password, String userName) async{
      var completer = new Completer();

      Map<String, dynamic> data = new Map<String, dynamic>();
      data["username"] = username;
      data["password"] = password;
      data["user_name"] = userName;

      await helpers.sendPost(data, globals.config["serverBasePath"] + globals.config["createUserRoute"]);

      completer.complete(await logIn(username, password));

      return completer.future;
    }

    void logOut(){
      window.localStorage["token"] = null;
      window.localStorage["user_name"] = null;
      window.localStorage["isAdmin"] = null;
    }

    bool isLoggedIn(){
      return window.localStorage["token"] != null;
    }

    bool isAdmin(){
      return isLoggedIn() && window.localStorage["isAdmin"] != null && (window.localStorage["isAdmin"] == "true");
    }

    String getUserName(){
      return window.localStorage["user_name"];
    }
  }

