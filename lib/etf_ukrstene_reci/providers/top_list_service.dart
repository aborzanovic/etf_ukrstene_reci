import 'dart:html';
import 'dart:convert';
import 'dart:async';

import 'package:angular2/core.dart';

import 'package:etf_ukrstene_reci/etf_ukrstene_reci/common/globals.dart' as globals;
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/common/helpers.dart' as helpers;

@Injectable()
class TopListService {

  Future<List<String>> getTopList() async{
    var completer = new Completer();

    Map<String, dynamic> data = new Map<String, dynamic>();

    Map<String, dynamic> response =
      await helpers.sendPost(data, globals.config["serverBasePath"] + globals.config["getTopListRoute"]);
    List<String> ret = new List();
    ret = response["top_list"];

    completer.complete(ret);

    return completer.future;
  }

  Future<Null> addResult(String userName, int result) async{
    var completer = new Completer();

    Map<String, dynamic> data = new Map<String, dynamic>();
    data["user_name"] = userName;
    data["result"] = result;

    Map<String, dynamic> response =
    await helpers.sendPost(data, globals.config["serverBasePath"] + globals.config["addTopListRoute"]);
    List<String> ret = new List();
    ret = response["top_list"];

    completer.complete(null);

    return completer.future;
  }
}