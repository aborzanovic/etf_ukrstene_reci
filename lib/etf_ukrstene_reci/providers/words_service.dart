  import 'dart:html';
  import 'dart:convert';
  import 'dart:async';

  import 'package:angular2/core.dart';

  import 'package:etf_ukrstene_reci/etf_ukrstene_reci/common/globals.dart' as globals;
  import 'package:etf_ukrstene_reci/etf_ukrstene_reci/common/helpers.dart' as helpers;
  import 'package:etf_ukrstene_reci/etf_ukrstene_reci/models/word.dart';

  @Injectable()
  class WordsService {
    Future<List<Word>> getWords(String searchWord) async{
      var completer = new Completer();

      List<Word> words = new List<Word>();
      Map<String, dynamic> data = new Map<String, dynamic>();
      data["word"] = searchWord;

      Map<String, dynamic> response =
      await helpers.sendPost(data, globals.config["serverBasePath"] +
          globals.config["searchForWordRoute"]);
      response["words"].forEach(
          (val) => words.add(new Word(val["id"],
              val["word"], val["description"], val["description_short"]))
      );

      completer.complete(words);

      return completer.future;
    }

    Future<List<Word>> getRandomWords(int num, int len) async{
      var completer = new Completer();

      List<Word> words = new List<Word>();
      Map<String, dynamic> data = new Map<String, dynamic>();
      data["num"] = num;
      data["len"] = len;

      Map<String, dynamic> response =
      await helpers.sendPost(data, globals.config["serverBasePath"] + globals.config["getRandomWordsRoute"]);
      response["words"].forEach(
          (val) => words.add(new Word(val["id"], val["word"], val["description"], val["description_short"]))
      );

      completer.complete(words);

      return completer.future;
    }
  }