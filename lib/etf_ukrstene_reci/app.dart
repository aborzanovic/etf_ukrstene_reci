import 'dart:html';
import 'package:angular2/core.dart';
import 'package:angular2/router.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/components/admin/admin_home_component.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/components/forms/login_form_component.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/components/forms/new_account_form_component.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/components/guards/home_guard.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/components/home_component.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/components/quick_game_component.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/components/top_list_component.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/providers/auth_service.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/common/globals.dart' as globals;
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/common/helpers.dart' as helpers;

@Component(
  selector: 'app',
  templateUrl: 'app.html',
  providers: const [AuthService, HomeGuard, ROUTER_PROVIDERS],
  directives: const [HomeComponent, QuickGameComponent, LoginFormComponent, AdminHomeComponent, NewAccountFormComponent, TopListComponent, RouterOutlet, RouterLink]
)
@RouteConfig(const [
  const Route(
    path: '/',
    component: HomeComponent,
    name: "Home"
  ),
  const Route(
    path: '/quick-game',
    component: QuickGameComponent,
    name: 'QuickGame'
  ),
  const Route(
      path: '/login',
      component: LoginFormComponent,
      name: 'Login'
  ),
  const Route(
      path: '/admin',
      component: AdminHomeComponent,
      name: 'AdminHome'
  ),
  const Route(
      path: '/new-account',
      component: NewAccountFormComponent,
      name: 'NewAccount'
  ),
  const Route(
      path: '/top-list',
      component: TopListComponent,
      name: 'TopList'
  )
])
class App {
  Router r;
  AuthService _authService;

  App(Router r, AuthService _authService){
    this.r = r;
    globals.appRouter = this.r;

    this._authService = _authService;

    this.r.subscribe(
        (val){
          if(globals.onBeforeWindowUnloadSub != null) {
            globals.onBeforeWindowUnloadSub.cancel();
            globals.onBeforeWindowUnloadSub = null;
          }
          return false;
        }
    );
  }

  navigate(List<String> arguments){
    helpers.navigate(arguments);
  }

  logOut(){
    _authService.logOut();
    navigate(["Home"]);
  }

  bool isActive(List instruction) {
    return this.r.isRouteActive(this.r.generate(instruction));
  }

  String get userName{
    if(_authService.isLoggedIn()) {
      return _authService.getUserName();
    }
    else {
      return null;
    }
  }

  bool get isLoggedIn{
    return _authService.isLoggedIn();
  }

  bool get isAdmin{
    return _authService.isAdmin();
  }
}
