
  class Word {
    int id;
    String word, description,descriptionShort;

    Word(this.id, this.word, this.description, this.descriptionShort);

    String toString() => word;

    operator ==(Word w){
      return id == w.id;
    }
  }

