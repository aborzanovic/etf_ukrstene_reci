import 'dart:collection';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/models/board.dart';

class User {
  String username, password, userName;
  User(this.username, this.password, [this.userName]);
}
