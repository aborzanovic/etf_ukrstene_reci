import 'dart:collection';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/models/board.dart';

class Game {
  int id;
  Board board;
  HashMap<int, String> letters;
  HashMap<String, String> horizontalTooltips;
  HashMap<String, String> verticalTooltips;
  Game(this.id, this.board, this.letters, this.horizontalTooltips, this.verticalTooltips);
}
