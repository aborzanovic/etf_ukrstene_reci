class Board {
  int dimension;
  int activeFieldNum = -1;
  List<int> dimensionList;
  List<int> blackFields;

  Board(this.dimension){
    dimensionList = new List<int>();
    blackFields = new List<int>();
    for(int i = 0; i < dimension; i++){
      dimensionList.add(i);
    }
  }

  int getFieldNum(int x, int y){
    return y*dimension+x;
  }

  bool isBlackField(int x, [int y]){
    bool result = false;
    int inputField = y == null ? x : getFieldNum(x,y);
    blackFields.forEach(
        (field){
          if(field == inputField)
            result = true;
        }
    );
    return result;
  }

  addBlackField(int fieldNum){
    if(!blackFields.contains(fieldNum)){
      blackFields.add(fieldNum);
    }
  }

  removeBlackField(int fieldNum){
    if(blackFields.contains(fieldNum)){
      blackFields.remove(fieldNum);
    }
  }

  String toString() => "black fields: $blackFields";
}
