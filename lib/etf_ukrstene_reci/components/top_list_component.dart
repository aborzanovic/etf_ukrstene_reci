import 'dart:collection';
import 'dart:html';
import 'package:angular2/core.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/components/field_component.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/models/board.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/providers/board_service.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/providers/top_list_service.dart';
import 'package:ng_bootstrap/ng_bootstrap.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/common/helpers.dart' as helpers;

@Component(
    selector: 'top-list-component',
    templateUrl: 'top_list_component.html',
    providers: const [TopListService]
)
class TopListComponent{
  TopListService _topListService;
  List<String> topList;

  TopListComponent(this._topListService){
    _topListService.getTopList().then(
        (List<String> list) => topList = list
    );
  }
}