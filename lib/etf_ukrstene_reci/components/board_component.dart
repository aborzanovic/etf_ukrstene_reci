import 'dart:collection';
import 'dart:html';
import 'package:angular2/core.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/components/field_component.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/models/board.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/providers/board_service.dart';
import 'package:ng_bootstrap/ng_bootstrap.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/common/helpers.dart' as helpers;

@Component(
    selector: 'board-component',
    templateUrl: 'board_component.html',
    providers: const [BoardService],
    directives: const [FieldComponent, NG_BOOTSTRAP_DIRECTIVES]
)
class BoardComponent implements AfterViewInit{
  @ViewChildren(FieldComponent) QueryList<FieldComponent> fields;

  var subscription = null;
  BoardService _boardService;
  ElementRef elementRef;
  FieldComponent prevFieldComponent = null;

  double boardWidth = .0;

  @Input()
  Board board;

  @Input()
  bool playMode;

  @Input()
  int gameId;

  @Input()
  HashMap horizontalTooltips;

  @Input()
  HashMap verticalTooltips;

  @Output()
  EventEmitter gameOverEvent = new EventEmitter();

  @Output()
  EventEmitter errorEvent = new EventEmitter();

  BoardComponent(this._boardService, this.elementRef){
    window.onResize.listen(
        (Event event) => initBoard()
    );
  }

  initBoard(){
    boardWidth = (elementRef.nativeElement as Element)
        .querySelector(".board-container")
        .contentEdge.width;
  }

  setFieldVals(Map<int, String> vals){
    fields.forEach(
        (FieldComponent field){
          field.content = vals[field.fieldNum];
        }
    );
  }

  clearFieldVals(){
    if(fields != null) {
      fields.forEach(
          (FieldComponent field) {
        field.content = "";
      }
      );
    }
  }

  reset(){
    if(fields != null) {
      fields.forEach(
          (FieldComponent field) {
        field.isCorrect = false;
        field.isActive = false;
        field.isLocked = false;
        field.isWrong = false;
        field.isMarked = false;
        field.hideLeftTooltip();
        field.hideTopTooltip();
        field.content = "";
      }
      );
    }
  }

  registerBoardChange(event){
    FieldComponent fieldComponent = getField(event["fieldNum"]);

    if(event["eventType"] == "click") {
      if (playMode) {

        fieldComponent.isActive = !fieldComponent.isActive;
        if(prevFieldComponent != null && prevFieldComponent != fieldComponent) {
          prevFieldComponent.isActive = false;
          prevFieldComponent.unsubscribeToKeyboard();
        }

        unmarkAllFields();
        hideAllTooltips();

        if (fieldComponent.isActive) {
          board.activeFieldNum = event["fieldNum"];
          fieldComponent.subscribeToKeyboard();
          if (!board.isBlackField(event["fieldNum"]) && !fieldComponent.isLocked) {
            //obelezavamo sva polja ka gore
            FieldComponent topField = null;
            for (int i = event["fieldNum"] - board.dimension; i >= 0;
            i -= board.dimension) {
              if (board.isBlackField(i))
                break;
              topField = getField(i);
              topField.markField();
            }
            //obelezavamo sva polja ka dole
            for (int i = event["fieldNum"] + board.dimension; i <
                board.dimension * board.dimension; i += board.dimension) {
              if (board.isBlackField(i))
                break;
              getField(i).markField();
            }
            //obelezavamo sva polja ka desno
            for (int i = event["fieldNum"] + 1; i <
                board.dimension * (event["fieldNum"] ~/ board.dimension + 1);
            i++) {
              if (board.isBlackField(i))
                break;
              getField(i).markField();
            }
            //obelezavamo sva polja ka levo
            FieldComponent leftField = null;
            for (int i = event["fieldNum"] - 1; i >=
                board.dimension * (event["fieldNum"] ~/ board.dimension); i--) {
              if (board.isBlackField(i))
                break;
              leftField = getField(i);
              leftField.markField();
            }
            if (topField == null)
              topField = fieldComponent;
            topField.showTopTooltip();
            if (leftField == null)
              leftField = fieldComponent;
            leftField.showLeftTooltip();
          }
        }
        else {
          board.activeFieldNum = -1;
          fieldComponent.unsubscribeToKeyboard();
        }
      }
      else {
        fieldComponent.isBlack = !fieldComponent.isBlack;
        if (fieldComponent.isBlack) {
          board.addBlackField(event["fieldNum"]);
        }
        else {
          board.removeBlackField(event["fieldNum"]);
        }
      }
    }
    else if(event["eventType"] == "keyboard") {
      if(event["keyCode"] == KeyCode.DOWN) {
        int newFieldNum = event["fieldNum"] + board.dimension;
        if (newFieldNum < board.dimension * board.dimension) {
          getField(newFieldNum).fieldClick();
        }
      }
      else if(event["keyCode"] == KeyCode.UP) {
        int newFieldNum = event["fieldNum"] - board.dimension;
        if (newFieldNum >= 0) {
          getField(newFieldNum).fieldClick();
        }
      }
      else if(event["keyCode"] == KeyCode.RIGHT) {
        int newFieldNum = event["fieldNum"] + 1;
        if (newFieldNum < board.dimension * board.dimension) {
          getField(newFieldNum).fieldClick();
        }
      }
      else if(event["keyCode"] == KeyCode.LEFT) {
        int newFieldNum = event["fieldNum"] - 1;
        if (newFieldNum >= 0) {
          getField(newFieldNum).fieldClick();
        }
      }
      else if(!board.isBlackField(event["fieldNum"]) && !getField(event["fieldNum"]).isLocked && KeyCode.isCharacterKey(event["keyCode"])) {
        if(event["keyCode"] >= KeyCode.A && event["keyCode"] <= KeyCode.Z && event["keyCode"] != KeyCode.Y){
          processCharacterInput(helpers.convertKeyCodeToCyrillic(event["keyCode"]), event["fieldNum"], fieldComponent);
        }
        else if(
            event["keyCode"] == KeyCode.OPEN_SQUARE_BRACKET ||
            event["keyCode"] == KeyCode.CLOSE_SQUARE_BRACKET ||
            event["keyCode"] == KeyCode.SEMICOLON ||
            event["keyCode"] == KeyCode.SINGLE_QUOTE ||
            event["keyCode"] == KeyCode.BACKSLASH
        ) {
          processCharacterInput(helpers.convertKeyCodeToCyrillic(event["keyCode"]), event["fieldNum"], fieldComponent);
        }
      }
      else if((event["keyCode"] == KeyCode.DELETE || event["keyCode"] == KeyCode.BACKSPACE) && !getField(event["fieldNum"]).isLocked) {
        fieldComponent.updateContent("");
        fieldComponent.setNeutralFormatting();
      }
    }

    prevFieldComponent = fieldComponent;
  }

  processCharacterInput(String str, int fieldNum, FieldComponent fieldComponent){
    if(!board.isBlackField(fieldNum)) {
      fieldComponent.updateContent(str);
      _boardService.checkField(gameId, fieldNum, fieldComponent.getContent()).then(
          (val) {
        if (val) {
          bool horizontalCorrect = true, verticalCorrect = true;
          fieldComponent.setCorrectFormatting();
          int leftField = -1, rightField = -1, topField = -1, bottomField = -1;
          //obelezavamo sva polja ka gore
          for (int i = fieldNum - board.dimension; i >= 0; i -= board.dimension) {
            if (board.isBlackField(i))
              break;
            verticalCorrect = verticalCorrect && getField(i).isCorrect;
            topField = i;
          }
          if(topField == -1)
            topField = fieldNum;
          //obelezavamo sva polja ka dole
          for (int i = fieldNum + board.dimension; i < board.dimension * board.dimension; i += board.dimension) {
            if (board.isBlackField(i))
              break;
            verticalCorrect = verticalCorrect && getField(i).isCorrect;
            bottomField = i;
          }
          if(bottomField == -1)
            bottomField = fieldNum;
          //obelezavamo sva polja ka desno
          for (int i = fieldNum + 1; i < board.dimension * (fieldNum ~/ board.dimension + 1); i++) {
            if (board.isBlackField(i))
              break;
            horizontalCorrect = horizontalCorrect && getField(i).isCorrect;
            rightField = i;
          }
          if(rightField == -1)
            rightField = fieldNum;
          //obelezavamo sva polja ka levo
          for (int i = fieldNum - 1; i >=
              board.dimension * (fieldNum ~/ board.dimension); i--) {
            if (board.isBlackField(i))
              break;
            horizontalCorrect = horizontalCorrect && getField(i).isCorrect;
            leftField = i;
          }
          if(leftField == -1)
            leftField = fieldNum;

          if(horizontalCorrect)
            for(int i = leftField; i <= rightField; i++)
                getField(i).isLocked = true;
          if(verticalCorrect)
            for(int i = topField; i <= bottomField; i += board.dimension)
              getField(i).isLocked = true;

          //check if all done
          bool allDone = true;
          for(int i = 0; i < board.dimension*board.dimension; i++)
              allDone = allDone && (getField(i).isLocked || getField(i).isBlack);
          if(allDone)
            gameOverEvent.emit({
              "gameOver": true
            });
        }
        else {
          fieldComponent.setWrongFormatting();
          errorEvent.emit({
            "error": true
          });
        }
      }
      );
    }
  }

  FieldComponent getField(int fieldNum){
    FieldComponent ret = null;
    fields.forEach(
        (FieldComponent field){
          if(field.fieldNum == fieldNum)
            ret = field;
        }
    );
    return ret;
  }

  unmarkAllFields(){
    fields.forEach(
        (FieldComponent field){
          field.unmarkField();
        }
    );
  }

  hideAllTooltips(){
    fields.forEach(
        (FieldComponent field){
      field.hideLeftTooltip();
      field.hideTopTooltip();
    }
    );
  }

  @override
  ngAfterViewInit() {
    initBoard();
  }
}