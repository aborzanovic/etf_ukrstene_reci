import 'dart:async';
import 'dart:collection';
import 'dart:html';
import 'package:angular2/core.dart';
import 'package:angular2/router.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/common/globals.dart' as globals;
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/common/helpers.dart' as helpers;
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/components/board_component.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/components/guards/home_guard.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/models/board.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/models/game.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/providers/auth_service.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/providers/board_service.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/providers/game_service.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/providers/top_list_service.dart';

@Component(
    selector: 'quick-game-component',
    templateUrl: 'quick_game_component.html',
    providers: const [GameService, BoardService, AuthService, TopListService],
    directives: const [BoardComponent]
)
class QuickGameComponent implements OnInit {
  static const int MAX_RESULT = 100000;
  @ViewChild(BoardComponent) BoardComponent boardComponent;
  Game quickGame;
  GameService _gameService;
  AuthService _authService;
  TopListService _topListService;
  Timer timer;
  int hours = 0, minutes = 0, seconds = 0, errorNum = 0;
  bool gameOver = false;

  QuickGameComponent(this._gameService, this._authService, this._topListService){
    quickGame = new Game(0, new Board(0), new HashMap(), new HashMap(), new HashMap());
  }

  registerGameOver(event){
    unloadGame();
  }

  registerErrorOccurred(event){
    errorNum++;
  }

  int getTimeElapsed(){
    return hours * 3600 + minutes * 60 + seconds;
  }

  loadNewGame(){
    gameOver = false;
    hours = 0;
    minutes = 0;
    seconds = 0;
    errorNum = 0;
    boardComponent.reset();
    _gameService.getRandomGame().then((Game game) {
      quickGame = game;
      boardComponent.initBoard();
      timer = new Timer.periodic(new Duration(seconds: 1),
          (Timer timer){
        seconds++;
        if(seconds == 60){
          seconds = 0;
          minutes++;
        }
        if(minutes == 60){
          minutes = 0;
          hours++;
        }
      }
      );
    });

    globals.onBeforeWindowUnloadSub = window.onBeforeUnload.listen(
        (BeforeUnloadEvent event) => event.returnValue = "Jeste li sigurni da želite da napustite partiju?"
    );
  }

  unloadGame(){
    gameOver = true;
    timer.cancel();
    int result = MAX_RESULT - getTimeElapsed() - errorNum * 100;
    helpers.displayMessage("Игра је завршена, ваш резултат је: " + result.toString(), "success");
    if(_authService.isLoggedIn()){
      _topListService.addResult(_authService.getUserName(), result);
    }

    if(globals.onBeforeWindowUnloadSub != null)
      globals.onBeforeWindowUnloadSub.cancel();
    globals.onBeforeWindowUnloadSub = null;
  }

  @override
  ngOnInit() {
    loadNewGame();
  }
}