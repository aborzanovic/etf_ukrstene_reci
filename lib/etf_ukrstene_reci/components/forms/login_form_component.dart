import 'dart:async';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/common/helpers.dart' as helpers;
import 'package:angular2/core.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/models/user.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/providers/auth_service.dart';


@Component(
    selector: 'login-form',
    templateUrl: 'login_form_component.html'
)
class LoginFormComponent implements OnInit {
  final AuthService _authService;

  User user = new User("","");

  onSubmit() {
    _authService.logIn(user.username, user.password).then(
        (Map<String, bool> ret){
          if(ret["success"]){
            if(ret["isAdmin"])
              helpers.navigate(["AdminHome"]);
            else
              helpers.navigate(["Home"]);
          }
        }
    );
  }

  LoginFormComponent(this._authService);

  @override
  Future<Null> ngOnInit() async {

  }
}