import 'dart:async';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/common/helpers.dart' as helpers;
import 'package:angular2/core.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/models/user.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/providers/auth_service.dart';


@Component(
    selector: 'new-account-form',
    templateUrl: 'new_account_form_component.html'
)
class NewAccountFormComponent {
  final AuthService _authService;

  User user = new User("","","");

  onSubmit() {
    _authService.createAccount(user.username, user.password, user.userName).then(
        (success){
          if(success)
            helpers.navigate(["Home"]);
        }
    );
  }

  NewAccountFormComponent(this._authService);
}