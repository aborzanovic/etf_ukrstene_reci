import 'package:angular2/core.dart';
import 'package:angular2/router.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/common/helpers.dart' as helpers;
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/components/cube_rotater_component.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/components/guards/home_guard.dart';

@Component(
    selector: 'home-component',
    templateUrl: 'home_component.html',
    directives: const [CubeRotaterComponent]
)
@CanActivate(
    HomeGuard.canActivate
)
class HomeComponent {

  HomeComponent();

  navigate(List<String> arguments){
    helpers.navigate(arguments);
  }
}