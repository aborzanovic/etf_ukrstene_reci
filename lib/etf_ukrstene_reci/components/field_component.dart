import 'dart:html';
import 'package:angular2/core.dart';
import 'package:ng_bootstrap/ng_bootstrap.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/common/globals.dart' as globals;

@Component(
    selector: 'field-component',
    templateUrl: 'field_component.html',
    directives: const [NG_BOOTSTRAP_DIRECTIVES]
)
class FieldComponent implements AfterViewInit {
  @ViewChildren(BsTooltipComponent) QueryList<BsTooltipComponent> tooltips;

  BsTooltipComponent leftTooltip, topTooltip;

  String content = "";
  bool isCorrect = false, isWrong = false, isLocked = false;
  bool isMarked = false;

  @Input()
  String topTooltipMessage = "", leftTooltipMessage = "";

  @Input()
  double fieldDimension;

  @Input()
  bool isBlack;

  @Input()
  int fieldNum;

  @Input()
  bool isActive;

  @Input()
  int boardDimension;

  @Output()
  EventEmitter boardChange = new EventEmitter();

  subscribeToKeyboard(){
    unsubscribeToKeyboard();
    globals.onKeydownSub = window.onKeyDown.listen(
        (KeyboardEvent event) {
          event.preventDefault();
          KeyEvent keyEvent = new KeyEvent.wrap(event);
          boardChange.emit({
            "eventType": "keyboard",
            "keyCode": keyEvent.keyCode,
            "fieldNum": fieldNum
          });
        }
    );
  }

  unsubscribeToKeyboard(){
    if(globals.onKeydownSub != null)
      globals.onKeydownSub.cancel();
    globals.onKeydownSub = null;
  }

  fieldClick(){
    boardChange.emit({
      "eventType": "click",
      "fieldNum": fieldNum
    });
  }

  markField(){
    isMarked = true;
  }

  unmarkField(){
    isMarked = false;
  }

  updateContent(String char){
    content = char;
  }

  String getContent(){
    return content;
  }

  setCorrectFormatting(){
    isCorrect = true;
    isWrong = false;
  }

  setWrongFormatting(){
    isCorrect = false;
    isWrong = true;
  }

  setNeutralFormatting(){
    isCorrect = false;
    isWrong = false;
  }

  showTopTooltip(){
    if(topTooltip != null)
      topTooltip.show();
  }

  hideTopTooltip(){
    if(topTooltip != null)
      topTooltip.hide();
  }

  showLeftTooltip(){
    if(leftTooltip != null)
      leftTooltip.show();
  }

  hideLeftTooltip(){
    if(leftTooltip != null)
      leftTooltip.hide();
  }

  @override
  ngAfterViewInit() {
    tooltips.forEach(
        (BsTooltipComponent tooltip){
          if(tooltip.placement == "top"){
            topTooltip = tooltip;
          }
          else if(tooltip.placement == "left"){
            leftTooltip = tooltip;
          }
        }
    );
  }
}