import 'dart:async';
import 'dart:html';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/common/helpers.dart' as helpers;
import 'package:angular2/core.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/components/admin/words_list_component.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/models/user.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/models/word.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/providers/auth_service.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/providers/words_service.dart';
import 'package:ng_bootstrap/ng_bootstrap.dart';


@Component(
    selector: 'words-selected',
    templateUrl: 'words_selected_component.html',
    directives: const [WordsListComponent, NG_BOOTSTRAP_DIRECTIVES]
)
class WordsSelectedComponent{
  @Input()
  List<Word> words = new List<Word>();

  @Input()
  bool wordsRemovable;

  @Output()
  EventEmitter wordRemoved = new EventEmitter();

  WordsSelectedComponent();

  removeWord(event){
    Word word = (event["word"] as Word);
    wordRemoved.emit({
      "word": word
    });
  }
}