import 'dart:async';
import 'dart:html';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/common/helpers.dart' as helpers;
import 'package:angular2/core.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/components/admin/words_list_component.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/models/user.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/models/word.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/providers/auth_service.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/providers/words_service.dart';
import 'package:ng_bootstrap/ng_bootstrap.dart';


@Component(
    selector: 'words-search',
    templateUrl: 'words_search_component.html',
    directives: const [WordsListComponent, NG_BOOTSTRAP_DIRECTIVES],
    providers: const [WordsService]
)
class WordsSearchComponent{
  @Output()
  EventEmitter wordSelected = new EventEmitter();

  String word = "";
  List<Word> words = new List<Word>();
  Timer timer;

  WordsService _wordsService;

  WordsSearchComponent(this._wordsService);

  searchForWords(KeyboardEvent e){
    if(timer != null)
      timer.cancel();
    timer = new Timer(
        new Duration(milliseconds: 500),
        (){
          String searchWord = (e.target as InputElement).value.toString();
          if(searchWord != null && searchWord != "") {
            _wordsService.getWords(searchWord).then(
                (wordsFound) {
              words = wordsFound;
            }
            );
          }
          else{
            words.clear();
          }
          timer.cancel();
        }
    );
  }

  selectWord(event){
    Word word = (event["word"] as Word);
    wordSelected.emit({
      "word": word
    });
  }
}