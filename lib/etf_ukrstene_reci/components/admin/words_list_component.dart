import 'dart:async';
import 'dart:html';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/common/helpers.dart' as helpers;
import 'package:angular2/core.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/models/user.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/models/word.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/providers/auth_service.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/providers/words_service.dart';
import 'package:ng_bootstrap/ng_bootstrap.dart';


@Component(
    selector: 'words-list',
    templateUrl: 'words_list_component.html',
    directives: const [NG_BOOTSTRAP_DIRECTIVES]
)
class WordsListComponent{
  @Input()
  List<Word> words = new List<Word>();

  @Input()
  bool wordsRemovable;

  @Output()
  EventEmitter wordSelected = new EventEmitter();

  @Output()
  EventEmitter wordRemoved = new EventEmitter();

  WordsListComponent();

  removeWord(Word word){
    wordRemoved.emit({
      "word": word
    });
  }

  selectWord(Word word){
    wordSelected.emit({
      "word": word
    });
  }
}