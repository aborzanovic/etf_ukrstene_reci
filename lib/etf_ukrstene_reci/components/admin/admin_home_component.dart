import 'dart:async';
import 'dart:html';
import 'package:etf_ukrstene_reci/crossword_gen/lib/crossword_gen.dart';
import 'package:etf_ukrstene_reci/crossword_gen/lib/src/state.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/common/helpers.dart' as helpers;
import 'package:angular2/core.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/components/admin/words_list_component.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/components/admin/words_search_component.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/components/admin/words_selected_component.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/components/board_component.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/models/board.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/models/user.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/models/word.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/providers/auth_service.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/providers/board_service.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/providers/game_service.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/providers/words_service.dart';


@Component(
    selector: 'admin-home',
    templateUrl: 'admin_home_component.html',
    directives: const [WordsSearchComponent, WordsListComponent, WordsSelectedComponent, BoardComponent],
    providers: const [WordsService, GameService, BoardService]
)
class AdminHomeComponent implements AfterViewInit, OnInit{
  @ViewChild(BoardComponent) BoardComponent boardComponent;

  static const int MAX_BOARD_SIZE = 15, MIN_BOARD_SIZE = 3;

  WordsService _wordsService;
  GameService _gameService;

  int randomWordsNum = 0;
  String selectedRandomLength = "0";
  List<int> randomWordLengths;
  List<Word> selectedWords = new List<Word>();
  int boardSize = MIN_BOARD_SIZE, currResult = -1, currStep = 0, uploadAllResultsProgress = -1;
  bool resultUploadInProgress = false;
  List<Map<int, String>> results;
  Board board;

  CrosswordGen crosswordGen;

  AdminHomeComponent(this._wordsService, this._gameService){
    board = new Board(MIN_BOARD_SIZE);
    randomWordLengths = new List();
    for(int i = 1; i < 16; i++){
      randomWordLengths.add(i);
    }
    results = new List();
  }

  addRandomWords(){
    _wordsService.getRandomWords(randomWordsNum, int.parse(selectedRandomLength)).then(
        (List<Word> randomWords) => randomWords.forEach(
            (Word randomWord){
              if(!selectedWords.contains(randomWord))
                selectedWords.add(randomWord);
            }
        )
    );
  }

  removeAllWords(){
    selectedWords.clear();
  }

  enlargeBoard(){
    if(boardSize < MAX_BOARD_SIZE) {
      boardSize++;
      board = new Board(boardSize);
    }
  }

  reduceBoard(){
    if(boardSize > MIN_BOARD_SIZE) {
      boardSize--;
      board = new Board(boardSize);
    }
  }

  registerWordAdded(event){
    Word word = (event["word"] as Word);
    if(!selectedWords.contains(word))
      selectedWords.add(word);
  }

  registerWordRemoved(event){
    Word word = (event["word"] as Word);
    if(selectedWords.contains(word))
      selectedWords.remove(word);
  }

  loadNextResult(){
    if(results.length > 0){
      currResult = (currResult + 1) % results.length;
      loadResult(results[currResult]);
    }
  }

  loadPrevResult(){
    if(results.length > 0){
      currResult -= 1;
      if(currResult < 0)
        currResult = results.length-1;
      loadResult(results[currResult]);
    }
  }

  loadResult(Map<int, String> result){
    boardComponent.clearFieldVals();
    boardComponent.setFieldVals(result);
  }

  clearResults(){
    results.clear();
    boardComponent.clearFieldVals();
    currResult = -1;
    currStep = 0;
  }

  uploadCurrentResult() async{
    if(results.length > 0) {
      resultUploadInProgress = true;
      await uploadResult(currResult);
      resultUploadInProgress = false;
    }
  }

  uploadAllResults() async{
    uploadAllResultsProgress = -1;
    resultUploadInProgress = true;
    for(int i = 0; i < results.length; i++){
      await uploadResult(i);
      uploadAllResultsProgress = (i.roundToDouble() / results.length * 100).round();
    }
    resultUploadInProgress = false;
  }

  uploadResult(int resultNum) async{
    List<String> letters = new List();
    for(int i = 0; i < boardSize*boardSize; i++){
      letters.add(results[resultNum][i]);
    }
    await _gameService.uploadGame(letters, board.blackFields, boardSize);
  }

  runAlgorithmStep(){
    if(currStep < 0)
      return;
    if(currStep == 0){
      results.clear();
      boardComponent.clearFieldVals();
      currResult = -1;
      crosswordGen = new CrosswordGen(boardSize);
      board.blackFields.forEach(
          (int fieldNum) => crosswordGen.addBlackFieldToBoard(fieldNum)
      );
      selectedWords.forEach(
          (Word word) => crosswordGen.addWordToLex(word.word)
      );
      crosswordGen.initialize();
    }
    currStep++;
    State state = crosswordGen.nextStep();
    print(state);
    if(state == null){
      currStep = -1;
      boardComponent.clearFieldVals();
      for(int i = 0; i < crosswordGen.getResultNum(); i++){
        crosswordGen.board.setState(crosswordGen.nextResult());
        results.add(crosswordGen.board.letters);
      }
      int solutionsFound = crosswordGen.getResultNum();
      helpers.displayMessage("Крај алгоритма! Пронађено "+solutionsFound.toString()+" решења!", solutionsFound > 0 ? "success" : "warn");
    }
    else{
      if(state.isConsistent() && state.isFinal())
        helpers.displayMessage("Пронађено решење!", "success");
      crosswordGen.board.setState(state);
      loadResult(crosswordGen.board.letters);
    }
  }

  runWholeAlgorithm(ButtonElement button){
    results.clear();
    boardComponent.clearFieldVals();
    currResult = -1;
    currStep = 0;
    uploadAllResultsProgress = -1;
    button.setInnerHtml('Укрштенице се праве <i class="fa fa-spinner fa-spin fa-fw"></i>');
    button.setAttribute("disabled", "disabled");
    List<String> crosswordWords = new List();
    selectedWords.forEach(
        (Word word) => crosswordWords.add(word.word)
    );

    Stopwatch stopwatch = new Stopwatch()
    ..start();

    CrosswordGen.performWholeSimulation(boardSize, crosswordWords, board.blackFields).then(
        (List<Map<int, String>> res){
          stopwatch.stop();
          button.setInnerHtml('Направи укрштенице');
          button.attributes.remove("disabled");
          int solutionsFound = res.length;
          helpers.displayMessage("Пронађено "+solutionsFound.toString()+" решења!", solutionsFound > 0 ? "success" : "warn");
          helpers.displayMessage("Време генерисања: "+stopwatch.elapsedMilliseconds.toString()+" милисекунди!", "info");
          results = res;
        }
    );
  }

  runSingleStep(Element button){
    button.setInnerHtml('Укрштенице се праве...');
    crosswordGen = new CrosswordGen(boardSize);
    selectedWords.forEach(
        (Word word) => crosswordGen.addWordToLex(word.word)
    );

    board.blackFields.forEach(
        (int fieldNum) => crosswordGen.addBlackFieldToBoard(fieldNum)
    );

    crosswordGen.initialize();

    var step = null;
    do{
      step = crosswordGen.nextStep();
    }while (step != null);

    for(int i = 0; i < crosswordGen.getResultNum(); i++){
      crosswordGen.board.setState(crosswordGen.nextResult());
      print(crosswordGen.board);
    }
    button.setInnerHtml('Направи укрштенице');
  }

  @override
  ngAfterViewInit() {
    boardComponent.initBoard();
  }

  @override
  ngOnInit() {
    boardComponent.initBoard();
  }
}