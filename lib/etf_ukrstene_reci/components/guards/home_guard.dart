import 'dart:async';
import 'dart:html';
import 'package:angular2/core.dart';
import 'package:angular2/router.dart';

import 'package:etf_ukrstene_reci/etf_ukrstene_reci/common/globals.dart' as globals;

@Injectable()
class HomeGuard implements CanActivate{

  static bool canActivate(ComponentInstruction next, ComponentInstruction prev){
    bool canNavigate = true;
    if(prev != null && next != null && prev.routeName == "QuickGame" && next.routeName == "Home") {
      canNavigate = window.confirm(
          "Jeste li sigurni da želite da napustite partiju?");

      if(!canNavigate) {
        new Future.delayed(
            new Duration(milliseconds: 500),
            () => globals.appRouter.navigate(["QuickGame"])
        );
      }
      else{
        if(globals.onKeydownSub != null)
          globals.onKeydownSub.cancel();
        globals.onKeydownSub = null;
      }
    }

    return canNavigate;
  }

  @override
  Function get fn => canActivate;
}