import 'package:etf_ukrstene_reci/crossword_gen/lib/src/slot.dart';

class State {
  List<Slot> slots;

  State({State state, List<Slot> slots}){
    if(state != null)
      initBasedOnSlots(state.getSlots());
    else if(slots != null)
      initBasedOnSlots(slots);
    else
      this.slots = new List<Slot>();
  }

  initBasedOnSlots(List<Slot> slots){
    this.slots = new List();
    slots.forEach(
        (Slot item) => this.slots.add(new Slot(slot: item))
    );
  }

  addSlot(Slot s){
    slots.add(s);
  }

  Slot getSlot(int index){
    return slots.elementAt(index);
  }

  List<Slot> getSlots(){
    return slots;
  }

  int getSlotNum(){
    return slots.length;
  }

  sortSlots(){
    slots.sort(
        (Slot s1, Slot s2) => s2.getSize() - s1.getSize()
    );
  }

  List<State> generateSubStates(){
    Slot lockSlot = null;
    int slotIndex = -1;
    for(int i = 0; i < getSlotNum(); i++){
      if(!slots.elementAt(i).isLocked()){
        lockSlot = slots.elementAt(i);
        slotIndex = i;
        break;
      }
    }
    if(lockSlot == null)
      return null;
    List<State> substates = new List();
    lockSlot.resetIterator();
    while(lockSlot.hasNext()){
      State substate = new State(state: this);
      substate.getSlot(slotIndex).lockValue(lockSlot.getWordIterator());
      substates.add(substate);
      lockSlot.moveIterator();
    }
    substates.forEach(
        (State s) => s.arcConsistency()
    );

    return substates;
  }

  arcConsistency(){
    slots.forEach(
        (Slot s1){
          slots.forEach(
              (Slot s2){
                if(s1 != s2 && s1.getSize() == s2.getSize())
                  s1.removeSlotChoice(s2);
              }
          );
        }
    );
  }

  bool isConsistent(){
    bool ret = true;
    slots.forEach(
        (Slot s){
          if(s.isEmpty())
            ret = false;
        }
    );
    return ret;
  }

  bool isFinal(){
    bool ret = true;
    slots.forEach(
        (Slot s){
          if(!s.isLocked())
            ret = false;
        }
    );
    return ret;
  }

  String toString(){
    String ret = "";
    String devider = "";
    slots.forEach(
        (Slot s){
          ret += (devider+s.toString());
          devider = "\n";
        }
    );
    return ret + "\n" + (isConsistent() ? "consistent state" : "inconsistent state") + (isFinal() ? "\nsolution reached" : "");
  }

  operator ==(State s){
    for(int i = 0; i < slots.length; i++){
      if(!(slots.elementAt(i) == s.getSlots().elementAt(i)))
        return false;
    }
    return true;
  }
}