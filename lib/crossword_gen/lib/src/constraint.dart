import 'package:etf_ukrstene_reci/crossword_gen/lib/src/coordinate.dart';
import 'package:etf_ukrstene_reci/crossword_gen/lib/src/slot.dart';
import 'package:etf_ukrstene_reci/crossword_gen/lib/src/state.dart';

class Constraint {
  int firstSlotIndex, secondSlotIndex;
  Coordinate intersection;

  Constraint(int s1, int s2, Coordinate c){
    firstSlotIndex = s1;
    secondSlotIndex = s2;
    intersection = c;
  }

  applyToState(State s){
    Slot first = s.getSlot(firstSlotIndex), second = s.getSlot(secondSlotIndex), lockedSlot = null;
    if(first.isEmpty() || second.isEmpty())
      return;
    if(first.isLocked())
      lockedSlot = first;
    else if(second.isLocked())
      lockedSlot = second;
    if(lockedSlot != null){
      String c = lockedSlot.getCharacter(intersection, lockedSlot.getChosenIndex());
      Slot otherSlot = lockedSlot == first ? second : first;
      otherSlot.resetIterator();
      while(otherSlot.hasNext()){
        if(!(c == otherSlot.getCharacter(intersection, otherSlot.getWordIterator()))){
          otherSlot.removeValue();
        }
        otherSlot.moveIterator();
      }
    }
  }
}