import 'package:etf_ukrstene_reci/crossword_gen/lib/src/constraint.dart';
import 'package:etf_ukrstene_reci/crossword_gen/lib/src/state.dart';
import 'package:etf_ukrstene_reci/crossword_gen/lib/src/state_stack.dart';

class Backtrack {
  List<State> results;
  StateStack stack;
  List<Constraint> constraints;
  int resultIterator = 0;

  Backtrack(State s){
    results = new List();
    stack = new StateStack();
    constraints = new List();

    stack.push(new State(state: s));
  }

  State getNextResult(){
    if(results.length <= 0)
      return null;
    State ret = results.elementAt(resultIterator);
    resultIterator = (resultIterator + 1) % results.length;
    return ret;
  }

  int getResultIterator(){
    return resultIterator;
  }

  addConstraint(Constraint c){
    constraints.add(c);
  }

  State nextStep(){
    if(stack.isEmpty())
      return null;
    State currState = stack.pop();
    constraints.forEach(
        (Constraint c) => c.applyToState(currState)
    );
    if(currState.isConsistent()){
      List<State> substates = currState.generateSubStates();
      if(substates == null){
        if(!results.contains(currState))
          results.add(currState);
      }
      else{
        substates.forEach(
            (State s) => stack.push(s)
        );
      }
    }
    return currState;
  }

  List<State> getAllResults(){
    return results;
  }
}