import 'dart:collection';
import 'package:etf_ukrstene_reci/crossword_gen/lib/src/state.dart';

class StateStack {
  Queue<State> stack;

  StateStack(){
    stack = new Queue();
  }

  push(State node){
    stack.addLast(node);
  }

  State pop(){
    if(stack.isNotEmpty) {
      State ret = stack.removeLast();
      return ret;
    }
    else return null;
  }

  bool isEmpty(){
    return stack.isEmpty;
  }
}