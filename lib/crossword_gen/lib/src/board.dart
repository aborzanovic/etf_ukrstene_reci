import 'package:etf_ukrstene_reci/crossword_gen/lib/src/coordinate.dart';
import 'package:etf_ukrstene_reci/crossword_gen/lib/src/lexicon.dart';
import 'package:etf_ukrstene_reci/crossword_gen/lib/src/slot.dart';
import 'package:etf_ukrstene_reci/crossword_gen/lib/src/state.dart';

class Board{
  Map<int, String> letters;
  int size;
  Lexicon lex;
  List<Coordinate> blackFields;

  Board(int n, Lexicon lex){
    letters = new Map();
    blackFields = new List<Coordinate>();
    this.size = n;
    this.lex = lex;
  }

  bool isEnabled(Coordinate c){
    bool result = true;
    if(c.getX() < 0 || c.getY() < 0 || c.getX() >= size || c.getY() >= size)
      return false;
    blackFields.forEach(
        (blackCoord){
          if(blackCoord == c)
            result = false;
        }
    );
    return result;
  }

  addBlackField(Coordinate c){
    if(!blackFields.contains(c)){
      blackFields.add(c);
    }
  }

  removeBlackField(Coordinate c){
    if(blackFields.contains(c)){
      blackFields.remove(c);
    }
  }

  setLetterText(String letter, Coordinate c){
    letters[c.getY()*size+c.getX()] = letter;
  }

  setState(State s){
    s.getSlots().forEach(
        (Slot slot){
          if(slot.isLocked())
            slot.setWord(slot.getWord(slot.getChosenIndex()));
        }
    );
  }

  String toString(){
    String ret = "";
    for(int i = 0; i < size*size; i++){
      if(i > 0 && i % size == 0)
        ret += "\n";
      if(letters[i] != null)
        ret += letters[i];
      else
        ret += "";
      ret += "\t";
    }
    return ret;
  }
}