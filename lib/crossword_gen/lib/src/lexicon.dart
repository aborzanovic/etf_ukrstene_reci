class Lexicon{
  List<String> words;

  Lexicon(){
    words = new List();
  }

  addWord(String toAdd){
    String w = toAdd.toUpperCase();
    if(!words.contains(w))
      words.add(w);
  }

  int getLen(int index){
    return words.elementAt(index).length;
  }

  String getWord(int index){
    return words.elementAt(index);
  }

  List<int> getAllWithLength(int length){
    List<int> array = new List();
    for(int i = 0; i < words.length; i++)
    if(getLen(i) == length){
      array.add(i);
    }
    return array;
  }
}