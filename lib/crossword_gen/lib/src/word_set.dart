import 'package:etf_ukrstene_reci/crossword_gen/lib/src/lexicon.dart';

class WordSet{
  List<int> words;
  int setLength;
  Lexicon lex;

  WordSet({int setLength, Lexicon lex, WordSet set, List<int> words}){
    if(set != null){
      this.setLength = set.getSize();
      this.lex = set.getLexicon();
      this.words = new List.from(set.getWholeSet());
    }
    else{
      this.setLength = setLength;
      this.lex = lex;

      if(words != null)
        this.words = words;
      else
        this.words = new List(setLength);
    }
  }

  Lexicon getLexicon(){
    return lex;
  }

  int getSize(){
    return words.length;
  }

  int getSetLength(){
    return setLength;
  }

  initWordSet(List<int> words){
    this.words = words;
  }

  List<int> getWholeSet(){
    return words;
  }

  bool containsWord(int wordIndex){
    return words.contains(wordIndex);
  }

  bool addWord(int wordIndex){
    if(containsWord(wordIndex))
      return false;
    words.add(wordIndex);
    return true;
  }

  bool removeWord(int wordIndex){
    if(wordIndex >= 0 && wordIndex < words.length){
      words.removeAt(wordIndex);
      return true;
    }
    return false;
  }

  String getWord(int wordIndex){
    if(wordIndex >= 0 && wordIndex <= words.length)
      return lex.getWord(words.elementAt(wordIndex));
    else return null;
  }

  lockChoice(int index){
    if(index >= 0 && index < words.length){
      int wordIndex = words.elementAt(index);
      words.clear();
      words.add(wordIndex);
      words = words.sublist(0, 1);
    }
  }

  int getLockedChoice(){
    if(words.length == 1)
      return words.elementAt(0);
    else
      return null;
  }

  removeChoice(int choice){
    if(choice == null)
      return;
    for(int i = 0; i < words.length; i++)
      if(words.elementAt(i) == choice){
        words.removeAt(i);
        i--;
      }
  }

  String toString(){
    String ret = "";
    String devider = " ";
    words.forEach(
        (int i){
          ret += devider+lex.getWord(i);
          devider = ",";
        }
    );
    return ret;
  }

  operator ==(WordSet ws){
    if(words.length != ws.getWholeSet().length)
      return false;
    for(int i = 0; i < ws.getSize(); i++){
      if(!(words.elementAt(i) == ws.getWholeSet().elementAt(i)))
        return false;
    }
    return true;
  }
}
