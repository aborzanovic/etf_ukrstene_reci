import 'package:etf_ukrstene_reci/crossword_gen/lib/src/board.dart';
import 'package:etf_ukrstene_reci/crossword_gen/lib/src/coordinate.dart';
import 'package:etf_ukrstene_reci/crossword_gen/lib/src/lexicon.dart';
import 'package:etf_ukrstene_reci/crossword_gen/lib/src/word_set.dart';

class Slot {
  Coordinate start, end;
  int index, wordIterator = 0;
  Board board;
  WordSet wordSet;

  Slot({Slot slot, int index, Coordinate start, Coordinate end, Board board, Lexicon lex}){
    if(slot != null){
      this.index = slot.getIndex();
      this.start = slot.getStart();
      this.end = slot.getEnd();
      this.board = slot.getBoard();
      this.wordIterator = slot.getWordIterator();
      this.wordSet = new WordSet(set: slot.getWordSet());
    }
    else{
      this.index = index;
      this.start = start;
      this.end = end;
      this.board = board;

      wordSet = new WordSet(setLength: getSize(),lex: lex);
    }
  }

  String getCharacter(Coordinate c, int index){
    if(isInSlot(c)){
      String s = getWord(index);
      int ind = getIndexByCoordinate(c);
      return s[ind];
    }
    else return null;
  }

  bool isInSlot(Coordinate c){
    return c.getY() >= start.getY() && c.getY() <= end.getY() && c.getX() >= start.getX() && c.getX() <= end.getX();
  }

  int getIndexByCoordinate(Coordinate c){
    if(getOrientation() == Coordinate.VERTICAL_ORIENTATION)
      return c.getX() - start.getX();
    else if(getOrientation() == Coordinate.HORIZONTAL_ORIENTATION)
      return c.getY() - start.getY();
    else
      return -1;
  }

  int getWordIterator(){
    return wordIterator;
  }

  int getChosenIndex(){
    if(isLocked()){
      return 0;
    }
    return -1;
  }

  Coordinate getStart(){
    return start;
  }

  Coordinate getEnd(){
    return end;
  }

  Board getBoard(){
    return board;
  }

  resetIterator(){
    wordIterator = 0;
  }

  bool hasNext(){
    return wordIterator < wordSet.getSize();
  }

  moveIterator(){
    wordIterator++;
  }

  lockValue([int index]){
    if(index == null)
      wordSet.lockChoice(wordIterator);
    else
      wordSet.lockChoice(index);
  }

  removeValue(){
    if(wordSet.removeWord(wordIterator))
      wordIterator--;
  }

  String getWord(int index){
    return wordSet.getWord(index);
  }

  bool isLocked(){
    return wordSet.getSize() == 1;
  }

  bool isEmpty(){
    return wordSet.getSize() == 0;
  }

  removeSlotChoice(Slot s){
    wordSet.removeChoice(s.getWordSet().getLockedChoice());
  }

  initWordSet(List<int> ws){
    wordSet.initWordSet(ws);
  }

  Coordinate getIntersection(Slot s){
    Coordinate test = new Coordinate(x: start.getX(), y: s.getStart().getY(), size: start.getSystemSize());
    if(isInSlot(test) && s.isInSlot(test))
      return test;
    else{
      test = new Coordinate(x: s.getStart().getX(), y: start.getY(), size: start.getSystemSize());
      if(isInSlot(test) && s.isInSlot(test))
        return test;
    }
    return null;
  }

  int getOrientation(){
    return start.getOrientation(end);
  }

  int getSize(){
    return start.getDistance(end);
  }

  int getIndex(){
    return index;
  }

  WordSet getWordSet(){
    return wordSet;
  }

  setWordSet(WordSet set){
    wordSet = new WordSet(set: set);
  }

  bool setWord(String word){
    int slotLen = start.getDistance(end);
    if(word.length != slotLen){
      print("GRESKA: duzina reci: "+word.length.toString()+", a duzina slota [od "+start.toString()+" do "+end.toString()+"]: "+start.getDistance(end).toString());
      return false;
    }
    Coordinate delta;
    if(getOrientation() == Coordinate.HORIZONTAL_ORIENTATION)
      delta = new Coordinate(x: 0, y: 1, size: start.getSystemSize());
    else
      delta = new Coordinate(x: 1, y: 0, size: start.getSystemSize());
    Coordinate currCoord = new Coordinate(c: start);
    for(int i = 0; i < slotLen; i++){
      board.setLetterText(word[i],currCoord);
      currCoord.add(delta);
    }
    return true;
  }

  String StNdRdTh(int index){
    switch(index){
      case 1:
        return "st";
      case 2:
        return "nd";
      case 3:
        return "rd";
      default:
        return "th";
    }
  }

  String toString(){
    return "set for "+index.toString()+StNdRdTh(index)+" word ["+start.toString()+"-"+end.toString()+"] : "+wordSet.toString()+(isLocked() ? " -> solution!" : "");
  }

  operator ==(Slot s){
    return wordSet == s.getWordSet();
  }
}