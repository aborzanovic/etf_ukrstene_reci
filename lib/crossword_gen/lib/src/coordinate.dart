import 'dart:math';

class Coordinate{
  static final int NO_ORIENTATION = 0;
  static final int VERTICAL_ORIENTATION = 1;
  static final int HORIZONTAL_ORIENTATION = 2;

  int x,y,size;

  Coordinate({Coordinate c, int x, int y, int size}){
    if(c != null){
      this.x = c.getX();
      this.y = c.getY();
      this.size = c.getSystemSize();
    }
    else {
      this.x = x;
      this.y = y;
      this.size = size;
    }
  }

  int getX(){
    return x;
  }

  int getY(){
    return y;
  }

  add(Coordinate c){
    x+=c.getX();
    y+=c.getY();
  }

  int getSystemSize(){
    return size;
  }

  int getIndex(){
    return getX()*size + getY();
  }

  int getOrientation(Coordinate c){
    if(x == c.getX()){
      return HORIZONTAL_ORIENTATION;
    }
    else if(y == c.getY()){
      return VERTICAL_ORIENTATION;
    }
    else{
      return NO_ORIENTATION;
    }
  }

  int getDistance(Coordinate c){
    return (sqrt((x-c.getX())*(x-c.getX())+(y-c.getY())*(y-c.getY()))+1).round();
  }

  operator ==(Coordinate c){
    return x == c.getX() && y == c.getY();
  }

  String toString(){
    return "("+getX().toString()+":"+getY().toString()+")";
  }
}