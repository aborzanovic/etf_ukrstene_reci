library crossword_gen;

import 'dart:async';
import 'dart:html';
import 'dart:isolate';
import 'package:etf_ukrstene_reci/crossword_gen/lib/src/backtrack.dart';
import 'package:etf_ukrstene_reci/crossword_gen/lib/src/board.dart';
import 'package:etf_ukrstene_reci/crossword_gen/lib/src/constraint.dart';
import 'package:etf_ukrstene_reci/crossword_gen/lib/src/coordinate.dart';
import 'package:etf_ukrstene_reci/crossword_gen/lib/src/lexicon.dart';
import 'package:etf_ukrstene_reci/crossword_gen/lib/src/slot.dart';
import 'package:etf_ukrstene_reci/crossword_gen/lib/src/state.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/common/helpers.dart' as helpers;

class CrosswordGen{
  Board board;
  State boardState;
  int n;
  Lexicon lex;
  Backtrack backtrack;

  CrosswordGen(this.n){
    lex = new Lexicon();
    board = new Board(n, lex);
    boardState = new State();
  }

  addWordToLex(String word){
    lex.addWord(word);
  }

  addBlackFieldToBoard(int fieldNum){
    int y = (fieldNum/n).floor();
    int x = fieldNum - y*n;
    board.addBlackField(new Coordinate(x: x, y: y, size: n));
  }

  initialize(){
    initSlots();
    boardState.sortSlots();
    backtrack = new Backtrack(boardState);
    initConstraints();
  }

  initConstraints(){
    List<Slot> boardSlots = boardState.getSlots();
    for(int i = 0; i < boardSlots.length-1; i++){
      for(int j = i+1; j < boardSlots.length; j++){
        Coordinate intersection = boardSlots.elementAt(i).getIntersection(boardSlots.elementAt(j));
        if(intersection != null)
          backtrack.addConstraint(new Constraint(i, j, intersection));
      }
    }
  }

  Slot getSlot(int index){
    return boardState.getSlot(index);
  }

  int getSlotNum(){
    return boardState.getSlotNum();
  }

  void initSlots(){
    int slotNum = 0;
    Coordinate start, end;
    for(int i = 0; i < n; i++){
      start = end = null;
      for(int j = 0; j < n; j++){
        Coordinate currCoord =  new Coordinate(x: i, y: j, size: n);
        if(!board.isEnabled(new Coordinate(x: i, y: j-1, size: n)) && board.isEnabled(currCoord)){
          start = currCoord;
        }
        if(!board.isEnabled(new Coordinate(x: i, y: j+1, size: n)) && board.isEnabled(currCoord) && start != null){
          end = currCoord;
          slotNum++;
          Slot tmpSlot = new Slot(index: slotNum, start: start, end: end, board: board, lex: lex);
          boardState.addSlot(tmpSlot);
          tmpSlot.initWordSet(lex.getAllWithLength(tmpSlot.getSize()));
        }
      }
      for(int j = 0; j < n; j++){
        start = end = null;
        if(!board.isEnabled(new Coordinate(x: i-1, y: j, size: n)) && board.isEnabled(new Coordinate(x: i, y: j, size: n))){
          start = new Coordinate(x: i, y: j, size: n);
        }
        for(int r = i; r < n; r++){
          Coordinate currCoord =  new Coordinate(x: r, y: j, size: n);
          if(!board.isEnabled(new Coordinate(x: r+1, y: j, size: n)) && board.isEnabled(currCoord) && start != null){
            end = currCoord;
            slotNum++;
            Slot tmpSlot = new Slot(index: slotNum,start: start, end: end, board: board, lex: lex);
            boardState.addSlot(tmpSlot);
            tmpSlot.initWordSet(lex.getAllWithLength(tmpSlot.getSize()));
            break;
          }
        }
      }
    }
  }

  int getResultNum(){
    return backtrack.getAllResults().length;
  }

  int getCurrResultNum(){
    return backtrack.getResultIterator() + 1;
  }

  State nextResult(){
    return backtrack.getNextResult();
  }

  State nextStep(){
    return backtrack.nextStep();
  }

  static Future<List<Map<int, String>>> performWholeSimulation(int n, List<String> words, List<int> blackFields) async{
    var completer = new Completer();
    ReceivePort myPort = new ReceivePort();
    SendPort workerPort;
    List<Map<int, String>> results = new List();

    myPort.listen(
        (msg){
          if(workerPort == null) {
            workerPort = msg;
            workerPort.send(n);
            workerPort.send(words);
            workerPort.send(blackFields);
            workerPort.send(true);
          }
          else {
            if(msg is bool && msg == true) { completer.complete(results); }
            else { results.add(msg as Map<int, String>); }
          }
        }
    );
    String workerUri;
    if(helpers.isDartVM())
      workerUri = "simulation_worker.dart";
    else
      workerUri = "simulation_worker.dart.js";
    Isolate.spawnUri(Uri.parse(workerUri), [], myPort.sendPort);
    return completer.future;
  }
}
