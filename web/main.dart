import 'package:angular2/platform/browser.dart';
import 'package:angular2/router.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/app.dart';
import 'package:etf_ukrstene_reci/config/default_browser.dart';
import 'package:etf_ukrstene_reci/etf_ukrstene_reci/common/globals.dart' as globals;

import 'package:angular2/src/core/reflection/reflection.dart' show reflector;
import 'package:angular2/src/core/reflection/reflection_capabilities.dart' show ReflectionCapabilities;

  main() {
    loadConfig().then(
        (Map config) {
            globals.config = config;
            reflector.reflectionCapabilities = new ReflectionCapabilities();
            bootstrap(App);
        },
        onError: (error) => print(error)
    );
  }

