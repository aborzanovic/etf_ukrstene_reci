  import 'dart:isolate';

  import 'package:etf_ukrstene_reci/crossword_gen/lib/crossword_gen.dart';
  import 'package:etf_ukrstene_reci/crossword_gen/lib/src/state.dart';

  main(List<String> args, SendPort sendPort) async{
    var myPort = new ReceivePort();
    int messageCnt = 0;
    CrosswordGen crosswordGen;
    sendPort.send(myPort.sendPort);
    myPort.listen(
        (msg) {
          switch(messageCnt){
            case 0:
              crosswordGen = new CrosswordGen(msg);
              break;
            case 1:
              List<String> words = msg;
              words.forEach((String word) => crosswordGen.addWordToLex(word));
              break;
            case 2:
              List<int> blackFields = msg;
              blackFields.forEach((int fieldNum) => crosswordGen.addBlackFieldToBoard(fieldNum));
              break;
            case 3:
              crosswordGen.initialize();
              State step = null;
              do{
                step = crosswordGen.nextStep();
              }while (step != null);
              for(int i = 0; i < crosswordGen.getResultNum(); i++){
                crosswordGen.board.setState(crosswordGen.nextResult());
                sendPort.send(crosswordGen.board.letters);
              }
              sendPort.send(true);
          }
          messageCnt++;
        }
    );
  }

