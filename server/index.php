<?php
set_time_limit(0);
ob_start();

require_once("application/system/Autoloader.php");

$Request = substr(filter_input(INPUT_SERVER, 'REQUEST_URI'), strlen(Config::PATH));

$RequestedRoute = null;
foreach (Routes::get() as $Route) {
    if (preg_match($Route['pattern'], $Request, $Arguments)) {
        $RequestedRoute = $Route;
        break;
    }
}
unset($Arguments[0]);
$Arguments = array_values($Arguments);

$controllerClass = $RequestedRoute['controller']."Controller";
require_once("application/controllers/".$controllerClass.".php");

$controller = new $controllerClass;

if (method_exists($controller, 'open'))
	$controller->open();

if (method_exists($controller, $RequestedRoute['action']))
	call_user_func_array([$controller, $RequestedRoute['action']], $Arguments);
else die("Controller $controllerClass doesn't have the action ".$RequestedRoute['action']);

if (method_exists($controller, 'close'))
	$controller->close();

$viewData = null;
if (method_exists($controller, 'getData'))
	$viewData = $controller->getData();

echo json_encode($viewData);