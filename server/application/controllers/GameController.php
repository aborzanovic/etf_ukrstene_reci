<?php

class GameController extends Controller{

	public function open(){
		parent::open();
	}

	public function close(){
		parent::close();
	}

	public function getRandomGame(){
        $game_db = GameModel::getRandomGame();
        $game = [];
        $game["letters"] = $game_db->letters;
        $game["id"] = $game_db->id;
        $game["board_id"] = $game_db->board_id;
        $game["tooltips"] = [];

        $tooltips_arrays = json_decode($game_db->tooltips);
        foreach($tooltips_arrays as $key_array => $tooltips_array){
            foreach($tooltips_array as $key => $val){
                $game["tooltips"][$key_array][$key] = WordModel::getWord($val)->description;
            }
        }
	    $this->setViewData('game', $game);
	}

	public function addGame(){
	    $lettersArray = $this->getInputVar("letters");
        $blackFieldsArray = array_map("intval", $this->getInputVar("blackFields"));
        $dimension = intval($this->getInputVar("dimension"));

        $tooltips = [
            0 => [],
            1 => []
        ];
        $tooltipSearch = "";
        //init vertical tooltips
        for($i = 0; $i < $dimension; $i++){
            if(!in_array($i, $blackFieldsArray)){
                $start = $j = $i;
                while($j < count($lettersArray) && !in_array($j, $blackFieldsArray)){
                    $tooltipSearch .= $lettersArray[$j];
                    $j += $dimension;
                }
                if($tooltipSearch !== ""){
                    $word = WordModel::findWord($tooltipSearch);
                    if($word != null)
                        $tooltips[0][$start] = $word->id;
                }
                $tooltipSearch = "";
            }
        }
        //init horizontal tooltips
        for($i = 0; $i < count($lettersArray); $i += $dimension){
            if(!in_array($i, $blackFieldsArray)){
                $start = $j = $i;
                while($j < ($i + $dimension) && !in_array($j, $blackFieldsArray)){
                    $tooltipSearch .= $lettersArray[$j];
                    $j++;
                }
                if($tooltipSearch !== ""){
                    $word = WordModel::findWord($tooltipSearch);
                    if($word != null)
                        $tooltips[1][$start] = $word->id;
                }
                $tooltipSearch = "";
            }
        }
        //init black field tooltips
        foreach($blackFieldsArray as $blackField){
            //init vertical tooltips
            $start = $j = $blackField + $dimension;
            while($j < count($lettersArray) && !in_array($j, $blackFieldsArray)){
                $tooltipSearch .= $lettersArray[$j];
                $j += $dimension;
            }
            if($tooltipSearch !== ""){
                $word = WordModel::findWord($tooltipSearch);
                if($word != null)
                    $tooltips[0][$start] = $word->id;
            }
            $tooltipSearch = "";

            //init horizontal tooltips
            $start = $j = $blackField + 1;
            while($j < (intval($blackField / $dimension + 1) * $dimension) && !in_array($j, $blackFieldsArray)){
                $tooltipSearch .= $lettersArray[$j];
                $j++;
            }
            if($tooltipSearch !== ""){
                $word = WordModel::findWord($tooltipSearch);
                if($word != null)
                    $tooltips[1][$start] = $word->id;
            }
            $tooltipSearch = "";
        }
        ksort($tooltips[0]);
        ksort($tooltips[1]);

	    GameModel::addGame($lettersArray, $dimension, $blackFieldsArray, $tooltips);
        $this->addInfoMessage("Игра успешно додата!");
    }
}