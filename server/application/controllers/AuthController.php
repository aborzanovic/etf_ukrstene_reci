<?php
require("application/vendor/firebase-jwt/JWT.php");
require("application/vendor/firebase-jwt/ExpiredException.php");
require("application/vendor/firebase-jwt/BeforeValidException.php");
require("application/vendor/firebase-jwt/SignatureInvalidException.php");

use \Firebase\JWT\JWT;

class AuthController extends Controller{

	public function open(){
		parent::open();
	}

	public function close(){
		parent::close();
	}

	public function determineSession($jwt){
	    try{
            $decoded = (array) JWT::decode($jwt, Config::APP_KEY, array('HS256'));
            return $decoded;
        }
        catch(Exception $e){
            $this->addWarnMessage("Невалидна сесија!");
            $this->addWarnMessage($e->getMessage());
            return false;
        }
	}

	public function createSession(){
	    $username = $this->getInputVar('username');
	    $password = $this->getInputVar('password');
        $token = array(
            "iss" => Config::BASE,
            "sub" => "auth token",
            "aud" => Config::BASE,
            "exp" => (time() + 2*60*60*1000)
        );

        if($username !== null && $password !== null){
            $user = AuthModel::getUserByUsername($username);
            if($user != null && $user != false && password_verify($password, $user ->password) === true){
                $token['admin'] = $user->admin;
                $jwt = JWT::encode($token, Config::APP_KEY, 'HS256');
                $this->setViewData('token', $jwt);
                $this->setViewData('isAdmin', $user->admin);
                $this->setViewData('user_name', $user->name);
                $this->addSuccessMessage("Исправни подаци за пријаву!");
            }
            else{
                $this->setViewData('token', '');
                $this->addMessage("Неисправни подаци за пријаву!");
            }
        }
        else{
            $this->setViewData('token', '');
            $this->addMessage("Неисправни подаци за пријаву!");
        }
	}

	public static function key_gen($key){
	    return password_hash($key, PASSWORD_BCRYPT);
	}
}