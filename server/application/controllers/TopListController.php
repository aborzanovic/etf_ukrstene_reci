<?php

class TopListController extends Controller{

	public function open(){
		parent::open();
	}

	public function close(){
		parent::close();
	}

	public function getTopList(){
        $topList = TopListModel::getTopList();
        $processedList = [];
        foreach($topList as $item){
            $processedList[] = "".$item->user_name." - ".$item->score;
        }
	    $this->setViewData('top_list', $processedList);
	}

	public function addResult(){
	    $user_name = $this->getInputVar("user_name");
	    $result = $this->getInputVar("result");
        $topList = TopListModel::addResult($user_name, $result);
    }
}