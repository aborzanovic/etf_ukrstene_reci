<?php

/**
 * osnovna klasa kontrolera
 */
class Controller{

	/**
	 * niz u kome kontroler cuva rezultat svojih f-ja, kasnije se ukljucuje u view
	 * @var array
	 */
	private $data = [];

	private $input_data = [];

	private $messages = [];

	/**
	 * f-ja koja se izvrsava pre svih ostalih
	 */
	public function open(){
	    $this->input_data = (array) json_decode(file_get_contents('php://input'));
	}

	/**
	 * f-ja nakon svih ostalih, snima poslednji korisnicki zahtev ukoliko treba korisnika da vratimo korak unazad
	 */
	public function close(){
	}

	/**
	 * dodavanje poruke u sesiju
	 * @param $message
	 */

	protected function addMessage($message, $type = "info"){
        $this->messages[] = array("message" => $message, "type" => $type);
    }

    protected function addInfoMessage($message){
        $this->addMessage($message, "info");
    }

    protected function addWarnMessage($message){
        $this->addMessage($message, "warn");
    }

    protected function addSuccessMessage($message){
        $this->addMessage($message, "success");
    }

    protected function addErrorMessage($message){
        $this->addMessage($message, "error");
    }

	/**
	 * dohvatanje svih sesijskih poruka
	 * @return bool
	 */
	public function getMessages(){
		return $this->messages;
	}

	/**
	 * snimanje podatka u data niz
	 * @param $key
	 * @param $val
	 */
	protected function setViewData($key, $val){
		$this->data[$key] = $val;
	}

	/**
	 * dohvatanje svih podataka potrebnih za ucitvanje view-a
	 * @return array
	 */
	public function getData(){
		$this->data['messages'] = $this->getMessages();
		return $this->data;
	}

	public function getInputVar($key){
	    if(isset($this->input_data[$key]))
	        return $this->input_data[$key];
	    else
	        return null;
	}
}