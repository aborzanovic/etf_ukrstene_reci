<?php

class WordController extends Controller{

	public function open(){
		parent::open();
	}

	public function close(){
		parent::close();
	}

    public function convert(){
        WordModel::convertToCyrillic();
        $this->setViewData('converted', true);
    }

    public function searchForWords(){
        $searchWord = Helper::convert($this->getInputVar("word"));
        $this->setViewData('words', WordModel::findWords($searchWord));
    }

    public function getRandomWords(){
        $this->setViewData('words', WordModel::randomWords($this->getInputVar("num"), intval($this->getInputVar("len"))));
    }
}