<?php

class UserController extends Controller{

	public function open(){
		parent::open();

	}

	public function close(){
		parent::close();
	}

	public function createUserAccount(){
	    $username = $this->getInputVar('username');
        $password = $this->getInputVar('password');
        $user_name = $this->getInputVar('user_name');

        if(!UserModel::addUser($username, $password, $user_name))
            $this->addErrorMessage("Додавање корисника није успело!");
        else
            $this->addSuccessMessage("Додавање корисника је успело!");
	}
}