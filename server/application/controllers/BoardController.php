<?php

class BoardController extends Controller{

	public function open(){
		parent::open();
	}

	public function close(){
		parent::close();
	}

	public function getAllConfigurations(){
	    $configs = BoardModel::getAll();
        $this->setViewData('configs', $configs);
	}

	public function getConfiguration(){
	    $request_body = file_get_contents('php://input');
        $data = json_decode($request_body);
        $board = [];
        if($data->id !== null){
            $b_conf = BoardModel::getBoard($data->id);
            $board["dimension"] = $b_conf->dimension;
            $board["blackFields"] = $b_conf->blackFields;
	     }
	     $this->setViewData('board', $board);
	}

	public function checkField(){
        $request_body = file_get_contents('php://input');
        $data = json_decode($request_body);
        if($data->id !== null && $data->num !== null && $data->char !== null){
            $actualChar = GameModel::getLetterForPosition($data->id, $data->num);
            if($actualChar === false){
                $this->setViewData('valid', false);
            }
            else{
                $this->setViewData('valid',
                    strcmp(
                        mb_strtoupper($data->char, 'UTF-8'),
                        mb_strtoupper($actualChar, 'UTF-8')
                    ) === 0);
            }
        }
        else
            $this->setViewData('valid', false);
	}

	public function addBoard(){
	    $this->setViewData('add-board', BoardModel::addBoard());
	}
}