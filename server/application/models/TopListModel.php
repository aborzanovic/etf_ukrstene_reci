<?php
class TopListModel{
	private static $table = 'top_list';

	public static function getTopList(){
	    $SQL = 'SELECT * FROM '.self::$table.' ORDER BY score DESC LIMIT 100;';
        $prep = DB::getInstance()->prepare($SQL);
        $prep->execute();
        return $prep->fetchAll(PDO::FETCH_OBJ);
	}

	public static function addResult($user_name, $result){
        $SQL = 'INSERT INTO '.self::$table.' (user_name, score) VALUES (?,?);';
        $prep = DB::getInstance()->prepare($SQL);
        return $prep->execute([$user_name, $result]);
    }
}