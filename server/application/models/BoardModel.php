<?php
class BoardModel{
	private static $table = 'boards';

	public static function getBoard($id){
	    $SQL = 'SELECT * FROM '.self::$table.' WHERE id = ?;';
        $prep = DB::getInstance()->prepare($SQL);
        $prep->execute([$id]);
        $board_conf = $prep->fetch(PDO::FETCH_OBJ);
        if(!empty($board_conf->blackFields))
            $board_conf->blackFields = array_map('intval', explode(',', $board_conf->blackFields));
        else
            $board_conf->blackFields = [];
        return $board_conf;
	}

	public static function findBoard($dimension, $blackFields){
	    $SQL = 'SELECT * FROM '.self::$table.' WHERE dimension = ? AND blackFields = ? LIMIT 1;';
	    $prep = DB::getInstance()->prepare($SQL);
        $prep->execute([$dimension, $blackFields]);
        return $prep->fetch(PDO::FETCH_OBJ);
	}

	public static function addBoard($dimension, $blackFieldsArray){
	    asort($blackFieldsArray);
        $blackFields = implode(",", $blackFieldsArray);

	    $board = self::findBoard($dimension, $blackFields);
	    if($board === false){
	        $SQL = 'INSERT INTO '.self::$table.' (dimension, blackFields) VALUES (?, ?);';
	        $prep = DB::getInstance()->prepare($SQL);
            $prep->execute([$dimension, $blackFields]);
            return DB::getInstance()->lastInsertId();
	    }
	    else{
	        return $board->id;
	    }
	}
}