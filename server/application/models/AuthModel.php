<?php
class AuthModel{
	private static $table = 'users';

	public static function getUserByUsername($username){
	    $SQL = 'SELECT * FROM '.self::$table.' WHERE username = ? LIMIT 1;';
        $prep = DB::getInstance()->prepare($SQL);
        $prep->execute([$username]);
        return $prep->fetch(PDO::FETCH_OBJ);
	}
}