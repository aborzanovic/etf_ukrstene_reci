<?php
class WordModel{
	private static $table = 'words';

	public static function convertToCyrillic(){
	    $LIMIT = 100;
	    $LIMIT_CURR = 0;
	    while(true){
            $SQL = 'SELECT * FROM words_lat LIMIT '.$LIMIT_CURR.', '.$LIMIT.';';
            $prep = DB::getInstance()->prepare($SQL);
            $prep->execute();
            $words = $prep->fetchAll(PDO::FETCH_OBJ);
            if(count($words) === 0)
                break;
            foreach($words as $word){
                $SQL_INS = 'INSERT INTO '.self::$table.' (word, description_short, description) VALUES (?, ?, ?);';
                $prep = DB::getInstance()->prepare($SQL_INS);
                $prep->execute([
                    Helper::convert($word->word),
                    Helper::convert($word->description_short),
                    Helper::convert($word->description)
                ]);
            }
            $LIMIT_CURR += $LIMIT;
        }
	}

	public static function getWord($id){
        $SQL = 'SELECT * FROM '.self::$table.' WHERE id = ? LIMIT 1;';
        $prep = DB::getInstance()->prepare($SQL);
        $prep->execute([$id]);
        return $prep->fetch(PDO::FETCH_OBJ);
    }

    public static function findWords($searchWord){
        $SQL = 'SELECT * FROM '.self::$table.' WHERE word like ? LIMIT 100;';
        $prep = DB::getInstance()->prepare($SQL);
        $prep->execute(['%'.$searchWord.'%']);
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }

    public static function findWord($searchWord){
        $SQL = 'SELECT * FROM '.self::$table.' WHERE word = ? LIMIT 1;';
        $prep = DB::getInstance()->prepare($SQL);
        $prep->execute([$searchWord]);
        return $prep->fetch(PDO::FETCH_OBJ);
    }

    public static function randomWords($num, $len){
        if($len > 0){
            $SQL = 'SELECT * FROM '.self::$table.' WHERE CHAR_LENGTH(word) = ? ORDER BY RAND() LIMIT ?;';
            $prep = DB::getInstance()->prepare($SQL);
            $prep->execute([$len, $num]);
        }
        else{
            $SQL = 'SELECT * FROM '.self::$table.' ORDER BY RAND() LIMIT ?;';
            $prep = DB::getInstance()->prepare($SQL);
            $prep->execute([$num]);
        }
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }
}