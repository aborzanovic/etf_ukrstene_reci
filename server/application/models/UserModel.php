<?php
class UserModel{
	private static $table = 'users';

    public static function addUser($username, $password, $user_name){
        $SQL = 'INSERT INTO '.self::$table.' (username, password, name, active, admin) VALUES (?,?,?,1,0);';
        $prep = DB::getInstance()->prepare($SQL);
        return $prep->execute([$username, AuthController::key_gen($password), $user_name]);
    }
}