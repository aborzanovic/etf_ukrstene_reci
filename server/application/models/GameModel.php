<?php
class GameModel{
	private static $table = 'games';

	public static function getRandomGame(){
	    $SQL = 'SELECT * FROM '.self::$table.' ORDER BY RAND() LIMIT 1;';
        $prep = DB::getInstance()->prepare($SQL);
        $prep->execute();
        return $prep->fetch(PDO::FETCH_OBJ);
	}

	public static function getLetterForPosition($id, $index){
        $SQL = 'SELECT * FROM '.self::$table.' WHERE id = ?;';
        $prep = DB::getInstance()->prepare($SQL);
        $prep->execute([$id]);
        $game = $prep->fetch(PDO::FETCH_OBJ);

        $letter_array = explode(",",$game->letters);

        if($index < 0 || $index >= count($letter_array)){
            return false;
        }
        else{
            return $letter_array[$index];
        }
    }

    public static function findGame($board_id, $letters){
        $SQL = 'SELECT * FROM '.self::$table.' WHERE board_id = ? AND letters = ? LIMIT 1;';
        $prep = DB::getInstance()->prepare($SQL);
        $prep->execute([$board_id, $letters]);
        return $prep->fetch(PDO::FETCH_OBJ);
    }

    public static function addGame($lettersArray, $dimension, $blackFieldsArray, $tooltips){
        $letters = implode(",", $lettersArray);

        $board_id = BoardModel::addBoard($dimension, $blackFieldsArray);

        $game = self::findGame($board_id, $letters);
        if($game === false){
            $SQL = 'INSERT INTO '.self::$table.' (board_id, letters, tooltips) VALUES (?, ?, ?);';
            $prep = DB::getInstance()->prepare($SQL);
            $prep->execute([$board_id, $letters, json_encode($tooltips)]);
        }
    }
}