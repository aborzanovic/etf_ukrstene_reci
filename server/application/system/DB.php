<?php

/**
 * implementacija baze podataka
 */
final class DB {
    /**
     * @var null
     */
    private static $db = null;

    /**
     * dohvata singleton instancu veze sa bazom
     * @return null|PDO
     */
    public static function getInstance() {
        if (self::$db === null) {
            self::$db = new PDO('mysql:host=' . Config::DB_HOST. ';dbname=' . Config::DB_BASE . ';charset=utf8', Config::DB_USER, Config::DB_PASS);
            self::$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        }

        return self::$db;
    }
}