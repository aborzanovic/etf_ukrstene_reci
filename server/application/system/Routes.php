<?php

/**
 * implementacija ruta
 */
final class Routes{
	/**
	 * vraca niz svih sistemskih ruta zajedno sa kontrolerima i f-jama koje treba da se izvrse
	 * @return array
	 */
	public static final function get(){
		return [
            [
                'pattern'    => '|^/session/create/?$|',
                'controller' => 'Auth',
                'action'     => 'createSession'
            ],
            [
                 'pattern'    => '|^/key-gen/(.*)/?$|',
                 'controller' => 'Auth',
                 'action'     => 'key_gen'
            ],
			[
	            'pattern'    => '|^/board/check/?$|',
	            'controller' => 'Board',
	            'action'     => 'checkField'
            ],
			[
                'pattern'    => '|^/board/get-configuration/?$|',
                'controller' => 'Board',
                'action'     => 'getConfiguration'
            ],
            [
                 'pattern'    => '|^/board/get-configuration/all/?$|',
                 'controller' => 'Board',
                 'action'     => 'getAllConfigurations'
            ],
            [
                 'pattern'    => '|^/game/add/?$|',
                 'controller' => 'Game',
                 'action'     => 'addGame'
            ],
            [
                 'pattern'    => '|^/game/get-random/?$|',
                 'controller' => 'Game',
                 'action'     => 'getRandomGame'
            ],
             [
                  'pattern'    => '|^/search-word/?$|',
                  'controller' => 'Word',
                  'action'     => 'searchForWords'
             ],
               [
                    'pattern'    => '|^/random-word/?$|',
                    'controller' => 'Word',
                    'action'     => 'getRandomWords'
               ],
               [
                    'pattern'    => '|^/user/add/?$|',
                    'controller' => 'User',
                    'action'     => 'createUserAccount'
               ],
               [
                    'pattern'    => '|^/top-list/get/?$|',
                    'controller' => 'TopList',
                    'action'     => 'getTopList'
               ],
               [
                    'pattern'    => '|^/top-list/add/?$|',
                    'controller' => 'TopList',
                    'action'     => 'addResult'
               ]
		];
	}
}