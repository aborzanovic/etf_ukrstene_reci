<?php
/**
 * autoload funkcija
 */
require_once('application/system/Helper.php');

spl_autoload_register(
	function($class) {
	    if (Helper::endswith($class, 'Controller')) {
	        require_once('application/controllers/' . $class . '.php');
	    } elseif (Helper::endswith($class, 'Model')) {
	        require_once('application/models/' . $class . '.php');
	    } else {
			require_once('application/system/' . $class . '.php');
		}
	}
);
