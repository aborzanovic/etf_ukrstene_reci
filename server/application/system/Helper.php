<?php

/**
 * helper klasa
 */
final class Helper{
	/**
	 * provera da li se jedan string zavrsava drugim
	 * @param $string
	 * @param $test
	 * @return bool
	 */
	public static final function endswith($string, $test) {
	    $strlen = strlen($string);
	    $testlen = strlen($test);
	    if ($testlen > $strlen) return false;
	    return substr_compare($string, $test, $strlen - $testlen, $testlen) === 0;
	}

    public static function convert($str) {
        $str = urldecode($str);
        $latin = [
            'a','b','c','č','ć','d','đ','e','f','g','h','i','j','k','l','m','n','o','p','r','s','š','t','u','v','z','ž',
            'A','B','C','Č','Ć','D','Đ','E','F','G','H','I','J','K','L','M','N','O','P','R','S','Š','T','U','V','Z','Ž'
        ];
        $cyrillic = [
            'а','б','ц','ч','ћ','д','ђ','е','ф','г','х','и','ј','к','л','м','н','о','п','р','с','ш','т','у','в','з','ж',
            'А','Б','Ц','Ч','Ћ','Д','Ђ','Е','Ф','Г','Х','И','Ј','К','Л','М','Н','О','П','Р','С','Ш','Т','У','В','З','Ж'
        ];
        $cyrillic_еrror = [
            'нј','лј','дж','НЈ','ЛЈ','ДЖ'
        ];
        $cyrillic_fixup = [
            'њ','љ','џ','Њ','Љ','Џ'
        ];
        return str_replace($cyrillic_еrror, $cyrillic_fixup, str_replace($latin, $cyrillic, $str));
    }
}